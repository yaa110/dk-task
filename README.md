# Task

Digikala Hiring Process: Task

## How to run

- Install dependencies: `composer install`
- Update autoloader: `composer dumpautoload -o`
- Enable `mysqli` extension in `php.ini`
- Run MySQL: `docker run --rm -p 3306:3306 -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=db -e MYSQL_USER=username -e MYSQL_PASSWORD=password -d mysql:5`
- Run Redis: `docker run --rm -d -p 6379:6379 redis`
- Run HTTP server: `composer start`

## Routes

- Send an SMS: `POST /sms/send?number=<NUMBER>&body=<BODY>`
- Search logs by phone number: `GET /sms/search?number=<NUMBER>`
- Report stats: `GET /sms/report`
- Retry sending failed messages: `POST /sms/retry`, this route could be called periodically using crontab.

## Missing

- Documentation
- Unit tests

## Test environment

```
php --version

PHP 7.4.1 (cli) (built: Dec 18 2019 12:59:28) ( NTS )
Copyright (c) The PHP Group
Zend Engine v3.4.0, Copyright (c) Zend Technologies
```
