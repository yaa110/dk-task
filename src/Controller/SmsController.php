<?php

namespace App\Controller;

use App\Config;
use App\Entity\SmsEntity;
use App\Log\Logger;
use App\Report\Reporter;
use App\Api\RequestApi;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class SmsController
{
    protected $logger = null;
    protected $reporter = null;
    protected $api = null;

    public function __construct(Logger $logger, Reporter $reporter, RequestApi $api)
    {
        $this->logger = $logger;
        $this->reporter = $reporter;
        $this->api = $api;
    }

    private function sendSMS($sms)
    {
        $candidateAPI = time() % count(Config::api);
        $effectiveAPI = $candidateAPI;
        $statusCode = Response::HTTP_OK;

        do {
            try {
                $this->api->do(Config::api[$effectiveAPI]["method"], Config::api[$effectiveAPI]["url"], $sms->number, $sms->body);
                $this->reporter->increment(Config::api[$effectiveAPI]["id"], $sms->number, true);
                $statusCode = Response::HTTP_OK;
                $sms->status = SmsEntity::StatusSent;
                break;
            } catch (\Throwable $e) {
                $this->reporter->increment(Config::api[$effectiveAPI]["id"], $sms->number, false);
                $statusCode = Response::HTTP_BAD_GATEWAY;
                $sms->status = SmsEntity::StatusFailed;
                \error_log($e);
            }
            $effectiveAPI = ($effectiveAPI + 1) % count(Config::api);
        } while ($effectiveAPI !== $candidateAPI);

        $sms->updatedAt = date("Y-m-d H:i:s");
        $this->logger->log($sms);

        return new Response('', $statusCode);
    }

    public function send($request): Response
    {
        $number = $request->query->get("number");
        $body = $request->query->get("body");

        if (empty($number) || empty($body)) {
            return new Response('', Response::HTTP_BAD_REQUEST);
        }

        $sms = SmsEntity::create($number, $body);
        $this->logger->log($sms);

        return $this->sendSMS($sms);
    }

    public function report($request): Response
    {
        $apiIDs = [];
        foreach (Config::api as &$api) {
            array_push($apiIDs, $api["id"]);
        }
        $resp = $this->reporter->stats($apiIDs, Config::report["count"]);
        return new JsonResponse($resp);
    }

    public function search($request): Response
    {
        $number = $request->query->get("number");

        if (empty($number)) {
            return new Response('', Response::HTTP_BAD_REQUEST);
        }

        $res = $this->logger->search($number);
        return new JsonResponse($res);
    }

    public function retry($request): Response
    {
        $rows = $this->logger->retry(Config::retry["count"]);

        foreach ($rows as $row) {
            $sms = SmsEntity::fromTuple($row);
            $this->sendSMS($sms);
        }

        return new Response('', Response::HTTP_OK);
    }
}
