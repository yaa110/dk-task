<?php

require_once __DIR__ . '/../vendor/autoload.php';

use App\Router;
use App\Api\RequestApi;
use App\Controller\SmsController;
use App\Log\MysqlLogger;
use App\Report\RedisReporter;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

$logger = new MysqlLogger();
$reporter = new RedisReporter();
$api = new RequestApi();
$controller = new SmsController($logger, $reporter, $api);

$routes = new RouteCollection();
$router = new Router($routes);

$router->post('sms_send', '/sms/send', function (Request $request) use ($controller) {
    return $controller->send($request);
});

$router->get('sms_report', '/sms/report', function (Request $request) use ($controller) {
    return $controller->report($request);
});

$router->get('sms_search', '/sms/search', function (Request $request) use ($controller) {
    return $controller->search($request);
});

$router->post('sms_retry', '/sms/retry', function (Request $request) use ($controller) {
    return $controller->retry($request);
});

$request = Request::createFromGlobals();
$matcher = new UrlMatcher($routes, new RequestContext());
$dispatcher = new EventDispatcher();
$dispatcher->addSubscriber(new RouterListener($matcher, new RequestStack()));
$controllerResolver = new ControllerResolver();
$argumentResolver = new ArgumentResolver();
$kernel = new HttpKernel($dispatcher, $controllerResolver, new RequestStack(), $argumentResolver);

try {
    $response = $kernel->handle($request);
} catch (NotFoundHttpException $e) {
    $response = new Response('', Response::HTTP_NOT_FOUND);
} catch (MethodNotAllowedHttpException $e) {
    $response = new Response('', Response::HTTP_METHOD_NOT_ALLOWED);
} catch (Throwable $e) {
    $response = new Response('', Response::HTTP_INTERNAL_SERVER_ERROR);
    error_log($e);
}

$response->send();
$kernel->terminate($request, $response);
