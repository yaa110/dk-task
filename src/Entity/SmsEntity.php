<?php

namespace App\Entity;

class SmsEntity
{
    const StatusCreated = 1;
    const StatusSent = 2;
    const StatusFailed = 3;

    public $id = null;
    public $number = null;
    public $body = null;
    public $createdAt = null;
    public $updatedAt = null;
    public $status = null;

    function __construct($id, $number, $body, $status, $createdAt, $updatedAt = null)
    {
        $this->id = $id;
        // FIXME: $number should be formatted to a standard phone number format (e.g. using libphonenumber)
        $this->number = $number;
        $this->body = $body;
        $this->status = $status;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
    }

    public static function create($number, $body): self
    {
        return new self(null, $number, $body, self::StatusCreated, date("Y-m-d H:i:s"));
    }

    public static function fromTuple($row): self
    {
        return new self($row["id"], $row["number"], $row["body"], $row["status"], $row["created_at"], $row["updated_at"]);
    }
}
