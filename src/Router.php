<?php

namespace App;

use Symfony\Component\Routing\Route;

class Router
{
    protected $routes = null;

    public function __construct($routes)
    {
        $this->routes = $routes;
    }

    private function add($name, $method, $url, $callable)
    {
        $this->routes->add($name, new Route($url, ['_controller' => $callable], [], [], "", [], $method, ""));
    }

    public function get($name, $url, $callable)
    {
        $this->add($name, "GET", $url, $callable);
    }

    public function post($name, $url, $callable)
    {
        $this->add($name, "POST", $url, $callable);
    }
}
