<?php

namespace App;

// FIXME: don't commit credentials
// FIXME: add more options to redis

class Config
{
    const mysql = [
        "host"      => "127.0.0.1",
        "user"      => "username",
        "password"  => "password",
        "db"        => "db",
    ];

    const redis = [
        "host"      => "127.0.0.1",
        "port"      => 6379,
    ];

    const api = [
        0 => [
            "id"     => "api1",
            "method" => "POST",
            "url"    => "http://127.0.0.1:8001/sms/send",
        ],
        1 => [
            "id"     => "api2",
            "method" => "POST",
            "url"    => "http://127.0.0.1:8002/sms/send",
        ],
    ];

    const report = [
        "count" => 10,
    ];

    const retry = [
        "count" => 10,
    ];
}
