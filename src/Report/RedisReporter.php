<?php

namespace App\Report;

use App\Report\Reporter;
use App\Config;
use Predis\Client;

class RedisReporter implements Reporter
{
    protected $client = null;

    public function __construct()
    {
        $this->client = new Client([
            'scheme' => 'tcp',
            'host'   => Config::redis["host"],
            'port'   => Config::redis["port"],
        ]);
    }

    public function increment($apiID, $number, $success)
    {
        $this->client->pipeline(function ($pipe) use ($apiID, $number, $success) {
            if ($success) {
                $pipe->incrby("total", 1);
                $pipe->incrby("api::{$apiID}::sent", 1);
                $pipe->zincrby("numbers", 1, $number);
            } else {
                $pipe->incrby("api::{$apiID}::failed", 1);
            }
        });
    }

    // FIXME: use an object and serialize it to JSON
    public function stats($apiIDs, $count)
    {
        $resps = $this->client->pipeline(function ($pipe) use ($apiIDs, $count) {
            foreach ($apiIDs as $apiID) {
                $pipe->get("api::{$apiID}::failed");
                $pipe->get("api::{$apiID}::sent");
            }
            $pipe->get("total");
            $pipe->zrevrange("numbers", 0, $count, "WITHSCORES");
        });
        $ret = [];
        $i = 0;
        foreach ($apiIDs as $apiID) {
            $ret["api"][$apiID]["failed"] = $resps[$i];
            $i += 1;
            $ret["api"][$apiID]["sent"] = $resps[$i];
            $i += 1;
        }
        $ret["total"] = $resps[$i];
        $i += 1;
        $ret["rank"] = $resps[$i];
        return $ret;
    }
}
