<?php

namespace App\Report;

interface Reporter
{
    public function increment($apiID, $number, $status);
    public function stats($apiIDs, $count);
}
