<?php

namespace App\Log;

use App\Log\Logger;
use App\Config;
use App\Entity\SmsEntity;

class MysqlLogger implements Logger
{
    protected $client = null;

    public function __construct()
    {
        $this->client = new \mysqli("p:" . Config::mysql["host"], Config::mysql["user"], Config::mysql["password"], Config::mysql["db"]);
        if ($this->client->connect_errno) {
            throw new \Exception("unable to connect to MySQL: " . $this->client->connect_error);
        }
        $this->prepare();
    }

    private function prepare()
    {
        // FIXME: use an ORM!
        $query = "CREATE TABLE IF NOT EXISTS logs (
            id INTEGER NOT NULL AUTO_INCREMENT,
            number VARCHAR(20) NOT NULL,
            body TEXT NOT NULL,
            status SMALLINT NOT NULL,
            created_at DATETIME,
            updated_at DATETIME DEFAULT NULL,
            PRIMARY KEY (id)
        )";
        if (!$this->client->query($query)) {
            throw new \Exception("unable to create logs table: " . $this->client->error);
        }
    }

    public function log(SmsEntity $sms)
    {
        if (is_null($sms->id)) {
            $query = "INSERT INTO logs(number, body, status, created_at) VALUES (?, ?, ?, ?)";
        } else {
            $query = "UPDATE logs SET status = ?, updated_at = ? WHERE id = ?";
        }
        $statement = $this->client->prepare($query);
        if (is_null($sms->id)) {
            $statement->bind_param("ssis", $sms->number, $sms->body, $sms->status, $sms->createdAt);
        } else {
            $statement->bind_param("isi", $sms->status, $sms->updatedAt, $sms->id);
        }
        try {
            $statement->execute();
            if (is_null($sms->id)) {
                $sms->id = $this->client->insert_id;
            }
        } catch (\Throwable $e) {
            throw new \Exception("unable to add log: " . $this->client->error);
        } finally {
            $statement->close();
        }
    }

    // FIXME: implement pagination and use deserialization
    public function search($number)
    {
        $query = "SELECT * FROM logs WHERE number LIKE CONCAT('%',?,'%')";
        $statement = $this->client->prepare($query);
        $statement->bind_param("s", $number);
        try {
            $statement->execute();
            $result = $statement->get_result();
            $ret = [];
            while ($row = $result->fetch_assoc()) {
                array_push($ret, $row);
            }
            return $ret;
        } catch (\Throwable $e) {
            throw new \Exception("unable to search logs: " . $this->client->error);
        } finally {
            $statement->close();
        }
    }

    public function retry($count)
    {
        $query = "SELECT * FROM logs WHERE status = ? LIMIT ?";
        $statement = $this->client->prepare($query);
        $status = SmsEntity::StatusFailed;
        $statement->bind_param("ii", $status, $count);
        try {
            $statement->execute();
            $result = $statement->get_result();
            $ret = [];
            while ($row = $result->fetch_assoc()) {
                array_push($ret, $row);
            }
            return $ret;
        } catch (\Throwable $e) {
            throw new \Exception("unable to retry logs: " . $this->client->error);
        } finally {
            $statement->close();
        }
    }
}
