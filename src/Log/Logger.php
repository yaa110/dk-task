<?php

namespace App\Log;

use App\Entity\SmsEntity;

interface Logger
{
    public function log(SmsEntity $sms);
    public function search($number);
    public function retry($count);
}
