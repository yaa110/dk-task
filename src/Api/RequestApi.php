<?php

namespace App\Api;

use Symfony\Component\HttpClient\HttpClient;

class RequestApi
{
    protected $client = null;

    public function do($method, $url, $number, $body)
    {
        $client = HttpClient::create();
        $response = $client->request($method, $url, [
            "query" => [
                "number" => $number,
                "body" => $body,
            ]
        ]);

        // FIXME: it is not a good criteria
        $statusCode = $response->getStatusCode();
        if ($statusCode >= 300) {
            throw new \Exception("Non-200 status code of API '{$url}'");
        }
    }
}
